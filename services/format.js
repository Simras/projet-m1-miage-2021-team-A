// Convert model to object and delete __v field
const formatGeneric = function (object, type) {
    if (object.length === 0) {
        return object;
    }
    object = object.toObject();
    delete object.__v;
    object.id = object._id;
    delete object._id;
    if (type === 'person') {
        object = formatFieldsPerson(object);
    } else if (type === 'book') {
        object = formatFieldsBook(object);
    } else if (type === 'game') {
        object = formatFieldsGame(object);
    } else if (type === 'movie') {
        object = formatFieldsMovie(object);
    } else if (type === 'user') {
        object = formatFieldsUser(object);
    }
    return object;
};

// Format specific fields of person object
const formatFieldsPerson = function (object) {
    const firstName = object.firstName;
    delete object.firstName;
    object.firstName = firstName;
    const lastName = object.lastName;
    delete object.lastName;
    object.lastName = lastName;
    const category = object.category;
    delete object.category;
    object.category = category;
    return object;
}

// Format specific fields of book object
const formatFieldsBook = function (object) {
    const title = object.title;
    delete object.title;
    object.title = title;
    const category = object.category;
    delete object.category;
    object.category = category;
    const year = object.year;
    delete object.year;
    object.year = year;
    const pageCount = object.pageCount;
    delete object.pageCount;
    object.pageCount = pageCount;
    const person = object.person;
    delete object.person;
    object.person = person;
    return object;
}

// Format specific fields of game object
const formatFieldsGame = function (object) {
    const title = object.title;
    delete object.title;
    object.title = title;
    const category = object.category;
    delete object.category;
    object.category = category;
    const year = object.year;
    delete object.year;
    object.year = year;
    const developmentTeam = object.developmentTeam;
    delete object.developmentTeam;
    object.developmentTeam = developmentTeam;
    const publisher = object.publisher;
    delete object.publisher;
    object.publisher = publisher;
    return object;
}

// Format specific fields of movie object
const formatFieldsMovie = function (object) {
    const title = object.title;
    delete object.title;
    object.title = title;
    const category = object.category;
    delete object.category;
    object.category = category;
    const duration = object.duration;
    delete object.duration;
    object.duration = duration;
    const year = object.year;
    delete object.year;
    object.year = year;
    const person = object.person;
    delete object.person;
    object.person = person;
    return object;
}

// Format specific fields of user object
const formatFieldsUser = function (object) {
    const username = object.username;
    delete object.username;
    object.username = username;
    const password = object.password;
    delete object.password;
    object.password = password;
    return object;
}

// Format person model to person object
const formatPerson = function (person, books, movies) {
    person = formatGeneric(person, 'person');
    if (books === undefined && movies === undefined) {
        return person;
    }
    if (books.length === 0 && movies.length === 0) {
        return person;
    }
    if (books.length > 0){
        person.books = formatBooks(books);
    }
    if (movies.length > 0){
        person.movies = formatMovies(movies);
    }
    return person;
};

// Format book model to book object
const formatBook = function (book) {
    book = formatGeneric(book, 'book');
    return book;
};

// Format game model to movie object
const formatGame = function (game) {
    game = formatGeneric(game, 'game');
    return game;
};

// Format movie model to movie object
const formatMovie = function (movie) {
    movie = formatGeneric(movie, 'movie');
    return movie;
};

// Format user model to user object
const formatUser = function (user) {
    user = formatGeneric(user, 'user');
    return user;
};

// Format books model to books object and adding specific fields
const formatBooks = function (books) {
    if (books.length === 0) {
        return books;
    }
    let formattedBooks = [];
    for (let book of books) {
        book = formatGeneric(book, 'book');
        formattedBooks.push(book);
    }
    return {
        size: formattedBooks.length,
        list: formattedBooks
    };
};

// Convert games model to games object and adding specific fields
const formatGames = function (games) {
    if (games.length === 0) {
        return games;
    }
    let formattedGames = [];
    for (let game of games) {
        game = formatGeneric(game, 'game');
        formattedGames.push(game);
    }
    return {
        size: formattedGames.length,
        list: formattedGames
    };
};

// Convert movies model to movies object and adding specific fields
const formatMovies = function (movies) {
    if (movies.length === 0) {
        return movies;
    }
    let formattedMovies = [];
    for (let movie of movies) {
        movie = formatGeneric(movie, 'movie');
        formattedMovies.push(movie);
    }
    return {
        size: formattedMovies.length,
        list: formattedMovies
    };
};

// Convert persons model to persons object and adding specs fields
const formatPersons = function (persons) {
    if (persons.length === 0) {
        return persons;
    }
    return {
        size: persons.length,
        list: persons
    };
};

// Export functions
module.exports = {
    formatPerson: formatPerson,
    formatPersons: formatPersons,
    formatBook: formatBook,
    formatBooks: formatBooks,
    formatGame: formatGame,
    formatGames: formatGames,
    formatMovie: formatMovie,
    formatMovies: formatMovies,
    formatUser: formatUser,
};
