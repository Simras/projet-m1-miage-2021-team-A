// Import models
const userModel = require('../models/user');

// Create entries in MongoDB database
const createUser = async function () {
    const user = new userModel({
        username: 'admin',
        password: 'password'
    });
    await user.save(function (error, usr) {
        if (error) {
            return console.error(error);
        }
        console.log('Created user', usr);
    });
};

// Export functions
module.exports = {loadExampleData: createUser};
