const mongoose = require('mongoose'),
    modelName = 'Person',
    schemaDefinition = new mongoose.Schema({
        firstName: {
            type: String,
            required: true
        },
        lastName: {
            type: String,
            required: true
        },
        category: {
            type: String,
            required: true
        }
    });

module.exports = mongoose.model(modelName, schemaDefinition);
