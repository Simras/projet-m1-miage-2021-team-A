const mongoose = require('mongoose'),
    modelName = 'Movie',
    schemaDefinition = new mongoose.Schema({
        title: {
            type: String,
            required: true
        },
        category: {
            type: String,
            required: true
        },
        year: {
            type: Number,
            required: true
        },
        duration: {
            type: Number,
            required: true
        },
        person: {
            type: mongoose.Schema.Types.ObjectId,
            required: true,
            ref: 'Person'
        }
    });

module.exports = mongoose.model(modelName, schemaDefinition);
