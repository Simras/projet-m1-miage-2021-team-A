const mongoose = require('mongoose'),
    modelName = 'Game',
    schemaDefinition = new mongoose.Schema({
        title: {
            type: String,
            required: true
        },
        category: {
            type: String,
            required: true
        },
        year: {
            type: Number,
            required: true
        },
        developmentTeam: {
            type: String,
            required: true
        },
        publisher: {
            type: String,
            required: true
        },
    });

module.exports = mongoose.model(modelName, schemaDefinition);
