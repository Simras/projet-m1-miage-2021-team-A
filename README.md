# ProgWeb NodeJS library application

Here is our web programming project.
It is an API for managing ratings of a library of books, games or movies allowing the addition and removal of games / movies by an administrator.

## Install

    npm install

## Run the app

    npm start

## Author

### Request

`POST /author/new`

    http://localhost:3000/author/new

### Response

    Create a new author

### Request

`GET /author/`

    http://localhost:3000/author/

### Response

    Return all authors

### Request

`GET /author/:id`

    http://localhost:3000/author/:id

### Response

    Return the author specify by the id

### Request

`UPDATE /author/:id`

### Response

    Update the author specify by the id

### Request

`DELETE /author/:id`

### Response

    Delete the author specify by the id

### Request

`POST /author/search`

### Response

    Return a list of authors corresponding to the search criteria

## Book

`POST /book/new`

    http://localhost:3000/book/new

### Response

    '200':
        description: successful operation
        Create a new book
    '500':
        description: Error creating book

### Request

`GET /book/`

    http://localhost:3000/book/

### Response

    '200':
        description: successful operation
        Return all books
    '500':
        description: Error gettings books

### Request

`GET /book/:id`

    http://localhost:3000/book/:id

### Response

    '200':
        description: successful operation
    Return the book specify by the id
    '500':
        description: Error gettings book

### Request

`UPDATE /book/:id`

### Response

    '200':
        description: successful operation
        Update the book specify by the id
    '500':
        description: Error gettings book

### Request

`DELETE /book/:id`

### Response

    '200':
        description: successful operation
        Delete the book specify by the id
    '500':
        description: Error gettings book

### Request

`POST /book/search`

### Response

    '200':
        description: successful operation
        Return a list of books corresponding to the search criteria
    '500':
        description: Error gettings books

## Game

`POST /game/new`

    http://localhost:3000/game/new

### Response

    '200':
        description: successful operation
        Create a new game
    '500':
        description: Error creating game

### Request

`GET /game/`

    http://localhost:3000/game/

### Response

    '200':
        description: successful operation
        Return all games
    '500':
        description: Error gettings games

### Request

`GET /game/:id`

    http://localhost:3000/game/:id

### Response

    '200':
        description: successful operation
    Return the game specify by the id
    '500':
        description: Error gettings game

### Request

`UPDATE /game/:id`

### Response

    '200':
        description: successful operation
        Update the game specify by the id
    '500':
        description: Error gettings game

### Request

`DELETE /game/:id`

### Response

    '200':
        description: successful operation
        Delete the game specify by the id
    '500':
        description: Error gettings game

### Request

`POST /game/search`

### Response

    '200':
        description: successful operation
        Return a list of games corresponding to the search criteria
    '500':
        description: Error gettings games

## Movie

`POST /movie/new`

    http://localhost:3000/movie/new

### Response

    '200':
        description: successful operation
        Create a new movie
    '500':
        description: Error creating movie

### Request

`GET /movie/`

    http://localhost:3000/movie/

### Response

    '200':
        description: successful operation
        Return all movies
    '500':
        description: Error gettings movies

### Request

`GET /movie/:id`

    http://localhost:3000/movie/:id

### Response

    '200':
        description: successful operation
    Return the movie specify by the id
    '500':
        description: Error gettings movie

### Request

`UPDATE /movie/:id`

### Response

    '200':
        description: successful operation
        Update the movie specify by the id
    '500':
        description: Error gettings movie

### Request

`DELETE /movie/:id`

### Response

    '200':
        description: successful operation
        Delete the movie specify by the id
    '500':
        description: Error gettings movie

### Request

`POST /movie/search`

### Response

    '200':
        description: successful operation
        Return a list of movies corresponding to the search criteria
    '500':
        description: Error gettings movies