// Import dependencies
const bodyParser = require('body-parser'),
    express = require('express'),
    fs = require('fs'),
    https = require('https'),
    mongoose = require('mongoose'),
    mongoDB = require('./services/mongoDB'),
    passport = require('passport');
require('./passport');

// Import routes
const bookRouter = require('./routes/book'),
    gameRouter = require('./routes/game'),
    movieRouter = require('./routes/movie'),
    personRouter = require('./routes/person'),
    userRouter = require('./routes/user');

// Import express dependencies
const app = express();

// Create a https server
https.createServer({
    key: fs.readFileSync('./certificate/server.key'),
    cert: fs.readFileSync('./certificate/server.cert')
}, app).listen(3000)

// MongoDB database connexion
mongoose.set('useUnifiedTopology', true);
mongoose.set('useCreateIndex', true);
mongoose.connect('mongodb://admin:Mong0DBpassword@cluster0-shard-00-00.enbpi.mongodb.net:27017,' +
    'cluster0-shard-00-01.enbpi.mongodb.net:27017,cluster0-shard-00-02.enbpi.mongodb.net:27017' +
    '/progWeb?ssl=true&replicaSet=atlas-axiaqy-shard-0&authSource=admin&retryWrites=true&w=majority',
    {useNewUrlParser: true}, function (error) {
        if (error) {
            console.log('Unable to connect to the MongoDB database', error);
        } else {
            console.log('Connected to the MongoDB database');
        }
    });
// Uncomment this line to initialize the database if not
// mongoDB.createUser();

// Settings to properly get requests and bodies from Postman
app.use(bodyParser.urlencoded({extended: false}));

// API general endpoints
app.use('/users', userRouter);
app.use('/persons', passport.authenticate('jwt', { session: false }), personRouter);
app.use('/books', passport.authenticate('jwt', { session: false }), bookRouter);
app.use('/games', passport.authenticate('jwt', { session: false }), gameRouter);
app.use('/movies', passport.authenticate('jwt', { session: false }), movieRouter);

// Export app
module.exports = app;
