// Import dependencies
const Book = require('../models/book'),
    Movie = require('../models/movie'),
    Person = require('../models/person'),
    format = require('../services/format'),
    express = require('express'),
    app = express(),
    router = express.Router(),
    cors = require('cors');

// Enable All CORS Requests
app.use(cors());

// Create new person
router.put('/new', async (req, res) => {
    try {
        const person = new Person({
            firstName: req.body.firstName,
            lastName: req.body.lastName,
            category: req.body.category
        });
        await person.save();
        return res.json(format.formatPerson(person));
    } catch {
        res.status(500);
        return res.json({error: 'Error creating person'});
    }
});

// Get all persons
router.get('/', async (req, res) => {
    try {
        let finalPersons = [];
        const persons = await Person.find();
        for (const person of persons) {
            const personBooks = await Book.find({person: person.id}).exec();
            const personMovies = await Movie.find({person: person.id}).exec();
            const formattedPerson = format.formatPerson(person, personBooks, personMovies);
            finalPersons.push(formattedPerson);
        }
        return res.json(format.formatPersons(finalPersons));
    } catch {
        res.status(500);
        return res.json({error: 'Error getting persons'});
    }
});

// Get person with id
router.get('/:id', async (req, res) => {
    try {
        const person = await Person.findById(req.params.id);
        const books = await Book.find({person: person.id}).exec();
        const movies = await Movie.find({person: person.id}).exec();
        const formattedPerson = format.formatPerson(person, books, movies);
        return res.json(formattedPerson);
    } catch {
        res.status(500);
        return res.json({error: 'Error getting person'});
    }
});

// Search persons with parameters
router.post('/search', async (req, res) => {
    try {
        const {firstName, lastName, category} = req.body;
        let finalPersons = [];
        let query = Person.find();
        if (firstName) {
            query = query.regex('firstName', new RegExp(firstName, 'i'));
        }
        if (lastName) {
            query = query.regex('lastName', new RegExp(lastName, 'i'));
        }
        if (category) {
            query = query.regex('category', new RegExp(category, 'i'));
        }
        const persons = await query.exec();
        if (persons.length > 0) {
            for (const person of persons) {
                const personBooks = await Book.find({person: person.id}).exec();
                const personMovies = await Movie.find({person: person.id}).exec();
                const formattedPerson = format.formatPerson(person, personBooks, personMovies);
                finalPersons.push(formattedPerson);
            }
            return res.json(format.formatPersons(finalPersons));
        } else {
            return res.json(format.formatPerson(finalPersons));
        }
    } catch {
        res.status(500);
        return res.json({error: 'Error getting books'});
    }
})
;

// Update person
router.put('/:id', async (req, res) => {
    try {
        let person = await Person.findById(req.params.id);
        person.firstName = req.body.firstName;
        person.lastName = req.body.lastName;
        await person.save();
        return res.json(format.formatPerson(person));
    } catch {
        res.status(500);
        return res.json({error: 'Error editing person'});
    }
});

// Delete person
router.delete('/:id', async (req, res) => {
    try {
        const person = await Person.findById(req.params.id);
        const books = await Book.find({person: person.id}).exec();
        if (books.length > 0) {
            res.status(500);
            return res.json({error: 'Impossible to delete person with books'});
        }
        await person.remove();
        return res.json({success: 'Person successfully deleted'});
    } catch {
        res.status(500);
        return res.json({error: 'Error deleting person'});
    }
});

// Export routes
module.exports = router;
