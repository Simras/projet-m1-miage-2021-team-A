// Import dependencies
const Book = require('../models/book'),
    Person = require('../models/person'),
    format = require('../services/format'),
    express = require('express'),
    app = express(),
    router = express.Router(),
    cors = require('cors');

// Enable All CORS Requests
app.use(cors());

// Create new book
router.put('/new', async (req, res) => {
    const {title, category, year, pageCount, person} = req.body;
    let author;
    try {
        author = await Person.findById(person);
    } catch {
        res.status(500);
        return res.json({error: 'Can not creat book, person does not exist'});
    }
    try {
        if (author.category !== 'author') {
            return res.json({error: 'The person is not an author'});
        }
        const book = new Book({
            title: title,
            category: category,
            year: Number(year),
            pageCount: Number(pageCount),
            person: person
        });
        await book.save();
        return res.json(format.formatBook(book));
    } catch {
        res.status(500);
        return res.json({error: 'Error creating book'});
    }
});

// Get all books
router.get('/', async (req, res) => {
    try {
        const books = await Book.find();
        return res.json(format.formatBooks(books));
    } catch {
        res.status(500);
        return res.json({error: 'Error getting books'});
    }
});

// Get book with id
router.get('/:id', async (req, res) => {
    try {
        const book = await Book.findById(req.params.id);
        return res.json(format.formatBook(book));
    } catch {
        res.status(500);
        return res.json({error: 'Error getting book'});
    }
});

// Search books with parameters
router.post('/search', async (req, res) => {
    const {title, category, year, filterYear, pageCount, filterPageCount, person} = req.body;
    try {
        let query = Book.find();
        if (title) {
            query = query.regex('title', new RegExp(title, 'i'));
        }
        if (category) {
            query = query.regex('category', new RegExp(category, 'i'));
        }
        if (year) {
            if (filterYear) {
                if (filterYear === 'before') {
                    query = query.lt('year', year);
                }
                if (filterYear === 'equal') {
                    query = query.lte('year', year);
                }
                if (filterYear === 'after') {
                    query = query.gt('year', year);
                }
            } else {
                query = query.lte('year', year);
            }
        }
        if (pageCount && filterPageCount) {
            if (filterPageCount === 'before') {
                query = query.lt('pageCount', pageCount);
            }
            if (filterPageCount === 'equal') {
                query = query.lte('pageCount', pageCount);
            }
            if (filterPageCount === 'after') {
                query = query.gt('pageCount', pageCount);
            } else {
                query = query.lte('pageCount', pageCount);
            }
        }
        if (person) {
            query = query.regex('person', new RegExp(person, 'i'));
        }
        const books = await query.exec();
        return res.json(format.formatBooks(books));
    } catch {
        res.status(500);
        return res.json({error: 'Error getting books'});
    }
});

// Update book
router.put('/:id', async (req, res) => {
        const {title, category, year, pageCount, person} = req.body;
        let author;
        try {
            author = await Person.findById(person);
        } catch {
            res.status(500);
            return res.json({error: 'Can not update book, person does not exist'});
        }
        try {
            if (author.category !== 'author') {
                return res.json({error: 'The person is not an author'});
            }
            let book = await Book.findById(req.params.id);
            if (title) {
                book.title = title;
            }
            if (category) {
                book.category = category;
            }
            if (year) {
                book.year = Number(year);
            }
            if (pageCount) {
                book.pageCount = Number(pageCount);
            }
            if (person) {
                book.person = person;
            }
            await book.save();
            return book.json(format.formatBook(book));
        } catch {
            res.status(500);
            return res.json({error: 'Error editing book'});
        }
    }
);

// Delete book
router.delete('/:id', async (req, res) => {
    try {
        const book = await Book.findById(req.params.id);
        await book.remove();
        return res.json({success: 'Book successfully deleted'});
    } catch {
        res.status(500);
        return res.json({error: 'Error deleting book'});
    }
});

// Export routes
module.exports = router;
