// Import dependencies
const Game = require('../models/game'),
    format = require('../services/format'),
    express = require('express'),
    app = express(),
    router = express.Router(),
    cors = require('cors');

// Enable All CORS Requests
app.use(cors());

// Create new game
router.put('/new', async (req, res) => {
    const {title, category, year, developmentTeam, publisher} = req.body;
    try {
        const game = new Game({
            title: title,
            category: category,
            year: Number(year),
            developmentTeam: developmentTeam,
            publisher: publisher
        });
        await game.save();
        return res.json(format.formatGame(game));
    } catch {
        res.status(500);
        return res.json({error: 'Error creating game'});
    }
});

// Get all games
router.get('/', async (req, res) => {
    try {
        const games = await Game.find();
        return res.json(format.formatGames(games));
    } catch {
        res.status(500);
        return res.json({error: 'Error getting games'});
    }
});

// Get game with id
router.get('/:id', async (req, res) => {
    try {
        const game = await Game.findById(req.params.id);
        return res.json(format.formatGame(game));
    } catch {
        res.status(500);
        return res.json({error: 'Error getting game'});
    }
});

// Search games with parameters
router.post('/search', async (req, res) => {
    const {title, category, year, filterYear, developmentTeam, publisher} = req.body;
    try {
        let query = Game.find();
        if (title) {
            query = query.regex('title', new RegExp(title, 'i'));
        }
        if (category) {
            query = query.regex('category', new RegExp(category, 'i'));
        }
        if (year) {
            if (filterYear) {
                if (filterYear === 'before') {
                    query = query.lt('year', year);
                }
                if (filterYear === 'equal') {
                    query = query.lte('year', year);
                }
                if (filterYear === 'after') {
                    query = query.gt('year', year);
                }
            } else {
                query = query.lte('year', year);
            }
        }
        if (developmentTeam) {
            query = query.regex('developmentTeam', new RegExp(developmentTeam, 'i'));
        }
        if (publisher) {
            query = query.regex('publisher', new RegExp(publisher, 'i'));
        }
        const games = await query.exec();
        return res.json(format.formatGames(games));
    } catch {
        res.status(500);
        return res.json({error: 'Error getting games'});
    }
});

// Update game
router.put('/:id', async (req, res) => {
    const {title, category, year, developmentTeam, publisher} = req.body;
    try {
        let game = await Game.findById(req.params.id);
        if(title) {
            game.title = title;
        }
        if(category) {
        game.category = category;
        }
        if(year) {
        game.year = Number(year);
        }
        if(developmentTeam) {
        game.developmentTeam = developmentTeam;
        }
        if(publisher) {
        game.publisher = publisher;
        }
        await game.save();
        return res.json(format.formatGame(game));
    } catch {
        res.status(500);
        return res.json({error: 'Error editing game'});
    }
});

// Delete game
router.delete('/:id', async (req, res) => {
    try {
        const game = await Game.findById(req.params.id);
        await game.remove();
        return res.json({success: 'Game successfully deleted'});
    } catch {
        res.status(500);
        return res.json({error: 'Error deleting game'});
    }
});

// Export routes
module.exports = router;
