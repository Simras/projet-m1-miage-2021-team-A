// Import dependencies
const Person = require('../models/person'),
    Movie = require('../models/movie'),
    format = require('../services/format'),
    express = require('express'),
    app = express(),
    router = express.Router(),
    cors = require('cors');

// Enable All CORS Requests
app.use(cors());

// Create new movie
router.put('/new', async (req, res) => {
    const {title, category, duration, year, person} = req.body;
    let producer;
    try {
        producer = await Person.findById(person);
    } catch {
        res.status(500);
        return res.json({error: 'Can not creat movie, person does not exist'});
    }
    try {
        if (producer.category !== 'producer') {
            return res.json({error: 'The person is not a producer'});
        }
        const movie = new Movie({
            title: title,
            category: category,
            year: Number(year),
            duration: Number(duration),
            person: person
        });
        await movie.save();
        return res.json(format.formatMovie(movie));
    } catch {
        res.status(500);
        return res.json({error: 'Error creating movie'});
    }
});

// Get all movies
router.get('/', async (req, res) => {
    try {
        const movies = await Movie.find();
        return res.json(format.formatMovies(movies));
    } catch {
        res.status(500);
        return res.json({error: 'Error getting movies'});
    }
});

// Get movie with id
router.get('/:id', async (req, res) => {
    try {
        const movie = await Movie.findById(req.params.id);
        return res.json(format.formatMovie(movie));
    } catch {
        res.status(500);
        return res.json({error: 'Error getting movie'});
    }
});

// Search movies with parameters
router.post('/search', async (req, res) => {
    const {title, category, year, filterYear, duration, filterDuration, person} = req.body;
    try {
        let query = Movie.find();
        if (title) {
            query = query.regex('title', new RegExp(title, 'i'));
        }
        if (category) {
            query = query.regex('category', new RegExp(category, 'i'));
        }
        if (year) {
            if (filterYear) {
                if (filterYear === 'before') {
                    query = query.lt('year', year);
                }
                if (filterYear === 'equal') {
                    query = query.lte('year', year);
                }
                if (filterYear === 'after') {
                    query = query.gt('year', year);
                }
            } else {
                query = query.lte('year', year);
            }
        }
        if (duration && filterDuration) {
            if (filterDuration === 'before') {
                query = query.lt('duration', duration);
            }
            if (filterDuration === 'equal') {
                query = query.lte('duration', duration);
            }
            if (filterDuration === 'after') {
                query = query.gt('duration', duration);
            }
        }
        if (person) {
            query = query.regex('person', new RegExp(person, 'i'));
        }
        const movies = await query.exec();
        return res.json(format.formatMovies(movies));
    } catch {
        res.status(500);
        return res.json({error: 'Error getting movies'});
    }
});

// Update movie
router.put('/:id', async (req, res) => {
    const {title, category, year, duration, person} = req.body;
    let producer;
    try {
        producer = await Person.findById(person);
    } catch {
        res.status(500);
        return res.json({error: 'Can not update movie, person does not exist'});
    }
    try {
        if (producer.category !== 'producer') {
            return res.json({error: 'The person is not a producer'});
        }
        let movie = await Movie.findById(req.params.id);
        if (title) {
            movie.title = title;
        }
        if (category) {
            movie.category = category;
        }
        if (year) {
            movie.year = Number(year);
        }
        if (duration) {
            movie.duration = Number(duration);
        }
        if (person) {
            movie.person = person;
        }
        await movie.save();
        return res.json(format.formatMovie(movie));
    } catch {
        res.status(500);
        return res.json({error: 'Error editing movie'});
    }
});

// Delete movie
router.delete('/:id', async (req, res) => {
    try {
        const movie = await Movie.findById(req.params.id);
        await movie.remove();
        return res.json({success: 'movie successfully deleted'});
    } catch {
        res.status(500);
        return res.json({error: 'Error deleting movie'});
    }
});

// Export routes
module.exports = router;
