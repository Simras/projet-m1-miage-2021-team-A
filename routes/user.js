// Import dependencies
const format = require('../services/format'),
    jwt = require('jsonwebtoken'),
    passport = require('passport'),
    express = require('express'),
    app = express(),
    router = express.Router(),
    cors = require('cors');

// Enable All CORS Requests
app.use(cors());

// Create new user
router.put(
    '/signup',
    passport.authenticate('signup', {session: false}),
    async (req, res) => {
        res.json({
            message: 'Signup successful',
            user : format.formatUser(req.user)
        });
    }
);

// Search user to generate JWT token
router.post(
    '/login',
    async (req, res, next) => {
        passport.authenticate(
            'login',
            async (err, user) => {
                try {
                    if (err || !user) {
                        res.status(500);
                        return res.json({error: 'An error occurred'});
                    }
                    req.login(
                        user,
                        {session: false},
                        async (error) => {
                            if (error) return next(error);
                            const body = {username: req.body.username, password: req.body.password};
                            const token = jwt.sign({user: body}, 'secretkey');
                            return res.json({token});
                        }
                    );
                } catch (error) {
                    res.status(500);
                    return res.json({error: 'An error occurred'});
                }
            }
        )(req, res, next);
    }
);

module.exports = router;
