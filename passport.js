// Import dependencies
const ExtractJWT = require('passport-jwt').ExtractJwt,
    JWTStrategy = require('passport-jwt').Strategy,
    LocalStrategy = require('passport-local').Strategy,
    passport = require('passport'),
    User = require('./models/user');

// Create a JWT authentication strategy
passport.use(
    new JWTStrategy(
        {
            secretOrKey: 'secretkey',
            jwtFromRequest: ExtractJWT.fromHeader('authorization')
        },
        async (token, done) => {
            try {
                return done(null, token.user);
            } catch (error) {
                done(error);
            }
        }
    )
);

// Save user
passport.use(
    'signup',
    new LocalStrategy({
            usernameField: 'username',
            passwordField: 'password'
        },
        async (username, password, done) => {
            try {
                const user = await User.create({username, password});
                return done(null, user);
            } catch (error) {
                done(error);
            }
        }
    )
);

// Search user
passport.use(
    'login',
    new LocalStrategy(
        {
            usernameField: 'username',
            passwordField: 'password'
        },
        async (username, password, done) => {
            try {
                const user = await User.findOne({ username, password });
                if (!user) {
                    return done(null, false, {message: 'User not found'});
                }
                return done(null, user, {message: 'Logged in Successfully'});
            } catch (error) {
                return done(error);
            }
        }
    )
);
